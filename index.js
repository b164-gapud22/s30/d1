const express = require('express');
// Mongoose is a package that allows creation of  Schemas to model ourdata structures
// Also has access to a number of methods for manipulation our database
const mongoose = require('mongoose')



const app = express();

const port = 3001;
// MongoDB Atlas Connection
// When we want to use local mongoDB/robo3t
// mongoose.connect("mongodb://localhost: 27017/databaseName")
mongoose.connect("mongodb+srv://magicgapud:magicgapud@magic.hb85z.mongodb.net/batch164_to-do?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	)
let db = mongoose.connection;

// connection error message
db.on("error", console.error.bind(console, "connection error"));
// connection is successful message
db.once("open", () => console.log("We're connected to the cloud database"))



app.use(express.json());

app.use(express.urlencoded({extended:true}));

// Mongoose Schemas
// Schema determine the structure of the documents to be written in the database
// act as blueprint to our data
// Use Schema() constructor of the Mongoose module to create a new Schema object.
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		// Default values are the predefined values for a field if we don't put any value.
		default: "pending"
	}
})

const Task = mongoose.model("Task", taskSchema);


// Create User Schema
const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

const User = mongoose.model("User", userSchema);




// Routes/endpoints

// Creating a new task

// Business Logic
/*
1. Add a functionality to check if there are duplicates tasks
-If the task already exist in the database, we return error
-if the task doesn't exist in the database, we add it in the database
	1. The task data will be coming from the request's body.
	2. Create a new Task object with field/property
	3. save the new object to our database.

*/

app.post("/tasks", (req, res) => {
	Task.findOne({ name: req.body.name }, (err, result) => {
		// If a document was found and the document's name matches the information sent via the client
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found");
		} else{
			// if no document was found
			// create a new task and save it to the database
			let newTask = new Task({
				name: req.body.name
			});

			newTask.save((saveErr, savedTask) => {
				// if there are errors in saving
				if(saveErr){
					return console.error(saveErr)
				} else{
					return res.status(201).send("New task created")
				}
			})


		}
	})
})


// Get all tasks
// Business Logic
/*
1. Find /retrieve all the documents
2. if an error is encounterd, print the error
3. if no errors are found, send a success status back to the client and return an array of documents

*/
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {

		if(err) {
			return console.log(err);
		}else{
			return res.status(200).json({
				dataFromMongoDB: result
			})
		}
	})
})


/*
Activity:
1. Create a User schema.
2. Create a User model.
3. Create a POST route that will access the "/signup" route that will create a user.
4. Process a POST request at the "/signup" route using postman to register a user.
5. Create a GET route that will return all users.
6. Process a GET request at the "/users" route using postman.
7. Create a git repository named S30.
8. Initialize a local git repository, add the remote link and push to git with the commit message of s30 Activity.
9. Add the link in Boodle named Express js Data Persistence via Mongoose ODM.


Business Logic in Creating a user "/signup"
1. Add a functionality to check if there are duplicates tasks
	- if the user already exists, return error Or "Already registered"
	- if the user does not exist, we add it on our database
		1. The user data will be coming from the req.body
		2. Create a new User object with a "username" and "password" fields/properties
		3. Then save, and add an error handling
*/

// Create a POST route that will access the "/signup" route that will create a user.
app.post("/signup", (req, res) => {
	User.findOne({username: req.body.username}, (err, result) => {
		if(result != null && result.username == req.body.username){
			return res.send('Username already taken.')
		}else{
			let newUser = new User({
				username: req.body.username
			});

			newUser.save((savedErr, savedUser) => {
				if(savedErr){
					return console.error(savedErr)
				}else{
					return res.status(201).send('New User Created');
				}
			})
		}
	})
})


// Create a GET route that will return all users.
/*
Business Logic in creating GET
1. Find /retrieve all the documents
2. if an error is encounterd, print the error
3. if no errors are found, send a success status back to the client and return an array of documents
*/
app.get(("/users"), (req, res) => {
	User.find({}, (err, result) => {
		if(err){
			return console.error(err)
		}else{
			return res.status(200).json({
				dataFromMongoDB: result
			})
		}
	})
})










app.listen(port, () => console.log(`Server running at port 	${port}`));

